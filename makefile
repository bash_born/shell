all: shell

shell: shell.o

	ld -m elf_x86_64 -s -o shell shell.o

shell.o: shell.asm

	nasm -f elf64 shell.asm
